# Topologische K-Theorie und Hopf-Invariante 1 

These are the lecture notes for the 'Topologische K-Theorie und Hopf-Invariante 1', taught in NONE at the University of Bonn.

The [latest version][1] is availabe as a pdf download via GitLab runner.
You can also have a look at the generated [log files][2] or visit the
[gl pages][3] index directly.

[1]: https://latexci.gitlab.io/qed/wuerzburg-22/k-theorie/2022_QED_K_Theorie_Hopf_Invariante.pdf
[2]: https://latexci.gitlab.io/qed/wuerzburg-22/k-theorie/2022_QED_K_Theorie_Hopf_Invariante.log
[3]: https://latexci.gitlab.io/qed/wuerzburg-22/k-theorie/
